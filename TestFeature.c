#include <stdio.h>
#include <stdlib.h>

union ilword {
    int n;
    union ilword* ptr;
    void(*f)();
};
typedef union ilword word;

word param[2];
int next_param = 0;

word r0 = {0};

word vg0 = {0};
word vg1 = {0};
word vg2 = {0};
void INIT();
void MAIN();
void Test_f();
void Test_g();
int main() {
    INIT();
    MAIN();
    return 0;
}

void INIT() {
    word vl[0];
    word r4 = {0};
    word r3 = {0};
    word r2 = {0};
    word r1 = {0};
    int p;
    for(p = 0; p <= -1 && p < 2; p++) {
        vl[p] = param[p];
    }
    next_param = 0;
INIT:
    r1.n = 1;
    r2.n = 0;
    vg0.ptr = calloc(r2.n, sizeof(word));
    r2.n = 0;
    vg1.ptr = calloc(r2.n, sizeof(word));
    r2.n = 2;
    vg2.ptr = calloc(r2.n, sizeof(word));
    r3 = vg2;
    r4.f = &Test_f;
    *(r3.ptr) = r4;
    r3.ptr = r3.ptr + r1.n;
    r4.f = &Test_g;
    *(r3.ptr) = r4;
    return;
}

void MAIN() {
    word vl[0];
    word r7 = {0};
    word r6 = {0};
    word r5 = {0};
    word r4 = {0};
    word r3 = {0};
    word r2 = {0};
    word r1 = {0};
    int p;
    for(p = 0; p <= -1 && p < 2; p++) {
        vl[p] = param[p];
    }
    next_param = 0;
MAIN:
    r1.n = 1;
    r2.ptr = calloc(r1.n, sizeof(word));
    *(r2.ptr) = vg2;
    r3 = *(r2.ptr);
    r4.n = 0;
    r5.ptr = r3.ptr + r4.n;
    r6 = *(r5.ptr);
    param[next_param++] = r2;
    (*(r6.f))();
    r7 = r0;
    if (r7.n == 0) goto main_0;
    goto main_1;
main_0:
main_1:
    return;
}

void Test_f() {
    word vl[3] = {0,0,0};
    word r13 = {0};
    word r12 = {0};
    word r11 = {0};
    word r10 = {0};
    word r9 = {0};
    word r8 = {0};
    word r7 = {0};
    word r6 = {0};
    word r5 = {0};
    word r4 = {0};
    word r3 = {0};
    word r2 = {0};
    word r1 = {0};
    int p;
    for(p = 0; p <= 2 && p < 2; p++) {
        vl[p] = param[p];
    }
    next_param = 0;
Test_f:
    vl[1].n = 0;
    vl[2].n = 1;
Test_f_0:
    r3.n = 11;
    r4.n = vl[2].n < r3.n;
    r10 = r4;
    r5 = *(vl[0].ptr);
    r6.n = 1;
    r7.ptr = r5.ptr + r6.n;
    r8 = *(r7.ptr);
    param[next_param++] = vl[0];
    param[next_param++] = vl[1];
    (*(r8.f))();
    r9 = r0;
    r10.n = r10.n - r9.n;
    if (r10.n == 0) goto Test_f_1;
    r10.n = 0;
    goto Test_f_2;
Test_f_1:
    r10.n = 1;
Test_f_2:
    if (r10.n == 0) goto Test_f_3;
    r11.n = vl[1].n + vl[2].n;
    vl[1] = r11;
    r12.n = 1;
    r13.n = vl[2].n + r12.n;
    vl[2] = r13;
    goto Test_f_0;
Test_f_3:
    r0.n = 0;
    return;
}

void Test_g() {
    word vl[2] = {0,0};
    int p;
    for(p = 0; p <= 1 && p < 2; p++) {
        vl[p] = param[p];
    }
    next_param = 0;
Test_g:
    printf("%d\n", vl[1]);
    r0.n = 0;
    return;
}

