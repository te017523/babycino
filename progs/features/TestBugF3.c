#include <stdio.h>
#include <stdlib.h>

union ilword {
    int n;
    union ilword* ptr;
    void(*f)();
};
typedef union ilword word;

word param[4];
int next_param = 0;

word r0 = {0};

word vg0 = {0};
word vg1 = {0};
word vg2 = {0};
void INIT();
void MAIN();
void Test_f();
void Test_x();
void Test_g();
int main() {
    INIT();
    MAIN();
    return 0;
}

void INIT() {
    word vl[0];
    word r4 = {0};
    word r3 = {0};
    word r2 = {0};
    word r1 = {0};
    int p;
    for(p = 0; p <= -1 && p < 4; p++) {
        vl[p] = param[p];
    }
    next_param = 0;
INIT:
    r1.n = 1;
    r2.n = 0;
    vg0.ptr = calloc(r2.n, sizeof(word));
    r2.n = 0;
    vg1.ptr = calloc(r2.n, sizeof(word));
    r2.n = 3;
    vg2.ptr = calloc(r2.n, sizeof(word));
    r3 = vg2;
    r4.f = &Test_f;
    *(r3.ptr) = r4;
    r3.ptr = r3.ptr + r1.n;
    r4.f = &Test_x;
    *(r3.ptr) = r4;
    r3.ptr = r3.ptr + r1.n;
    r4.f = &Test_g;
    *(r3.ptr) = r4;
    return;
}

void MAIN() {
    word vl[0];
    word r7 = {0};
    word r6 = {0};
    word r5 = {0};
    word r4 = {0};
    word r3 = {0};
    word r2 = {0};
    word r1 = {0};
    int p;
    for(p = 0; p <= -1 && p < 4; p++) {
        vl[p] = param[p];
    }
    next_param = 0;
MAIN:
    r1.n = 1;
    r2.ptr = calloc(r1.n, sizeof(word));
    *(r2.ptr) = vg2;
    r3 = *(r2.ptr);
    r4.n = 0;
    r5.ptr = r3.ptr + r4.n;
    r6 = *(r5.ptr);
    param[next_param++] = r2;
    (*(r6.f))();
    r7 = r0;
    if (r7.n == 0) goto main_0;
    goto main_1;
main_0:
main_1:
    return;
}

void Test_f() {
    word vl[3] = {0,0,0};
    word r13 = {0};
    word r12 = {0};
    word r11 = {0};
    word r10 = {0};
    word r9 = {0};
    word r8 = {0};
    word r7 = {0};
    word r6 = {0};
    word r5 = {0};
    word r4 = {0};
    word r3 = {0};
    word r2 = {0};
    word r1 = {0};
    int p;
    for(p = 0; p <= 2 && p < 4; p++) {
        vl[p] = param[p];
    }
    next_param = 0;
Test_f:
    vl[1].n = 0;
    vl[2].n = 1;
Test_f_0:
    r3 = *(vl[0].ptr);
    r4.n = 1;
    r5.ptr = r3.ptr + r4.n;
    r6 = *(r5.ptr);
    param[next_param++] = vl[0];
    param[next_param++] = vl[2];
    (*(r6.f))();
    r7 = r0;
    r13 = r7;
    if (r13.n == 1) goto Test_f_1;
    r8 = *(vl[0].ptr);
    r9.n = 2;
    r10.ptr = r8.ptr + r9.n;
    r11 = *(r10.ptr);
    param[next_param++] = vl[0];
    param[next_param++] = vl[1];
    (*(r11.f))();
    r12 = r0;
    r13 = r12;
Test_f_1:
    if (r13.n == 0) goto Test_f_2;
    printf("%d\n", vl[2]);
    goto Test_f_0;
Test_f_2:
    r0.n = 0;
    return;
}

void Test_x() {
    word vl[0];
    int p;
    for(p = 0; p <= -1 && p < 4; p++) {
        vl[p] = param[p];
    }
    next_param = 0;
Test_x:
    r0.n = 0;
    return;
}

void Test_g() {
    word vl[0];
    int p;
    for(p = 0; p <= -1 && p < 4; p++) {
        vl[p] = param[p];
    }
    next_param = 0;
Test_g:
    r0.n = 1;
    return;
}

