class TestBugF3 {

    // Bug:: When x is false and y is true, return false


    public static void main(String[] a) {
	if (new Test().f()) {} else {}
    }
}

class Test {

    public boolean f() {
	int result;
	int count;
	result = 0;
	count = 1;
	while ( this.x(count) || this.g(result)) {
	    System.out.println(count);
    }
	return false;
    }

    public boolean x(int n){
        return false;
    }

    public boolean g(int n) {
        return true;
    }

}
// if 1 is printed, means it returned true;
