class TestBugB3 {
    public static void main(String[] args){
        System.out.println(new Test().Start());
    }
}


class Test {
    public int Start() {
        int a;
        int b;
        int c;

        a = 1;
        b = 2;
        c = 2;

        if (c != a && a != b) {
            System.out.println(1);
        } else {
            System.out.println(0);
        }

    //Added return for successfull compile to test Bug
    return 0;

    }
}